# https://github.com/christianversloot/machine-learning-articles/blob/main/creating-a-multilayer-perceptron-with-pytorch-and-lightning.md
# https://github.com/karpathy/micrograd/blob/master/demo.ipynb

import random
import numpy as np
import matplotlib.pyplot as plt

import torch
from torch import nn

from torch.utils.data import TensorDataset, DataLoader
from torchvision import transforms

# make up a dataset

from sklearn.datasets import make_moons, make_blobs
X, y = make_moons(n_samples=300, noise=0.1)
# y = y*2 - 1 # make y be -1 or 1

### hard define the layers

class MLP(nn.Module):
    '''
    Multilayer Perceptron.
    '''
    def __init__(self):
        super().__init__()
        self.layers = nn.Sequential(
            nn.Flatten(),
            nn.Linear(2, 4),
            nn.ReLU(),
            nn.Linear(4, 4),
            nn.ReLU(),
            nn.Linear(4, 2)
        )

    def forward(self, x):
        '''Forward pass'''
        return self.layers(x)

# need to define a Dataset class

class Dataset(torch.utils.data.Dataset):
    '''Characterizes a dataset for PyTorch'''
    def __init__(self, features, labels):
        self.features = features
        self.labels = labels

    def __len__(self):
        '''Denotes the total number of samples'''
        return len(self.features)

    def __getitem__(self, index):
        '''Generates one sample of data'''
        # Select sample
        X = self.features[index, :]
        y = self.labels[index]

        X = X.astype(float)
        y = y.astype(float)

        return X, y

tensor_X = torch.Tensor(X)
tensor_y = torch.Tensor(y)
my_dataset = TensorDataset(tensor_X, tensor_y)
trainloader = DataLoader(my_dataset)

if __name__ == '__main__':
  
    # Set fixed random number seed
    torch.manual_seed(42)

#    dataset = Dataset(X, y)
#    trainloader = torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=True, num_workers=1)

    mlp = MLP()

    # Define the loss function and optimizer
    loss_function = nn.CrossEntropyLoss()
#    loss_function = nn.BCELoss()
    optimizer = torch.optim.Adam(mlp.parameters(), lr=1e-4)

    # Run the training loop
    for epoch in range(0, 100): # 5 epochs at maximum
    
        # Print epoch
        print(f'Starting epoch {epoch+1}')

        # Set current loss value
        current_loss = 0.0

        # Iterate over the DataLoader for training data
        for i, data in enumerate(trainloader, 0):

            # Get inputs
            inputs, targets = data
#            inputs.to(float)

            # Zero the gradients
            optimizer.zero_grad()

            # Perform forward pass
            outputs = mlp(inputs.float())

            # Compute loss
            loss = loss_function(outputs, targets.long())

            # Perform backward pass
            loss.backward()

            # Perform optimization
            optimizer.step()

            # Print statistics
            current_loss += loss.item()
            if i % 500 == 499:
                print('Loss after mini-batch %5d: %.3f' %
                    (i + 1, current_loss / 500))
                current_loss = 0.0
        
        if (epoch+1) % 5 == 0:
            # loss and accuracy for the epoch
            with torch.no_grad():
                outputs = mlp(tensor_X)
            y_pred_class = (outputs[:, 1] > 0).float()
            accuracy = np.mean(list(y_pred_class) == y)
            print(f'Epoch {epoch+1}: {loss.item() = :.4f}, {accuracy = :.4f}')

    # Process is complete.
    print('Training process has finished.')
