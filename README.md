# microgradR

Implementing Andrej Karpathy's [micrograd](https://github.com/karpathy/micrograd) in R. 

Currently using the [R6](https://r6.r-lib.org/articles/Introduction.html) package for object-oriented programming, specifically to achieve "pass by reference"-like functionality. 
